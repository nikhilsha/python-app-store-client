try:
    import ast
    import tkinter as tk
    import requests as req
    from tkinter import ttk

    # Setup
    window = tk.Tk()
    window.geometry('400x325')
    window.title('Python App Store Client')
    main_frame = tk.Frame(window)
    main_frame.pack(fill=tk.BOTH, expand=1)

    my_canvas = tk.Canvas(main_frame)
    my_canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    my_scrollbar = ttk.Scrollbar(
        main_frame, orient=tk.VERTICAL, command=my_canvas.yview)
    my_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    my_canvas.configure(yscrollcommand=my_scrollbar.set)
    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(
        scrollregion=my_canvas.bbox("all")))
    root = tk.Frame(my_canvas)
    my_canvas.create_window((0, 0), window=root, anchor="nw")

    def _on_mouse_wheel(event):
        my_canvas.yview_scroll(-1 * int((event.delta / 135)), "units")
    my_scrollbar.bind_all("<MouseWheel>", _on_mouse_wheel)
    # Main Code
    apps = req.get('http://127.0.0.1:5000/guiclient').content.decode('utf-8')
    apps = ast.literal_eval(apps)

    AddedApps = []

    def CopyText(copytext):
        window.clipboard_clear()
        window.clipboard_append(copytext)

    def AddApp(AppName, AppDescription, AppCodeForMac, AppCodeForWindows):
        global AddedApps
        AddedApps.append(tk.Label(root, text=AppName, font=('Arial', 16)))
        AddedApps.append(tk.Label(root, text=AppDescription))
        AddedApps.append(str(AppCodeForMac))
        AddedApps.append(str(AppCodeForWindows))
        AddedApps.append(tk.Button(root, text='Download ' + AppName + ' for Mac',
                                   command=lambda: CopyText(str(AppCodeForMac)), padx=50, pady=25))
        AddedApps.append(tk.Button(root, text='Download ' + AppName + ' for Windows',
                                   command=lambda: CopyText(str(AppCodeForWindows)), padx=50, pady=25))
        for i in range(len(AddedApps)):
            try:
                AddedApps[i].pack()
            except:
                pass

    def RefreshApps():
        global AddedApps, apps
        apps = req.get(
            'http://127.0.0.1:5000/guiclient').content.decode('utf-8')
        apps = ast.literal_eval(apps)
        for i in range(len(AddedApps)):
            try:
                AddedApps[i].pack_forget()
            except:
                pass
        AddedApps = []
        for i in range(len(apps)):
            AddApp(apps[i]['name'], apps[i]['description'],
                   apps[i]['Mac Code'], apps[i]['Windows Code'])
    RefreshImg = tk.PhotoImage(file='refresh.png')
    RefreshButton = tk.Button(root, image=RefreshImg,
                              command=RefreshApps)
    RefreshButton.pack()
    RefreshApps()

    root.mainloop()
except SyntaxError:
    import tkinter as tk
    from tkinter import messagebox
    listthing = ['haloo', 'hallo']
    a = messagebox.Message(
        title='hallo', message='hallo', type=messagebox.YESNOCANCEL, command=print, options='blah')
    a.show()
    root = tk.Tk()
    root.withdraw()
    messagebox.showinfo('There was a problem',
                        'There are no apps to load or the server is not running.')
    exit()
